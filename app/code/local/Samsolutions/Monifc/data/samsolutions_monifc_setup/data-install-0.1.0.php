<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer
    ->setConfigData('design/header/logo_src', 'images/logo.svg')
    ->setConfigData('design/header/logo_src_small', 'images/logo.svg')
;

$installer->endSetup();